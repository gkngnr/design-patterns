package patterns.decorator.shapes;

public class ShapeTest {

	public static void main(String[] args) {
		//Shape circle = new Circle();

//		Shape redCircle = new RedShapeDecorator(new Circle());
//
//		Shape redRectangle = new RedShapeDecorator(new Rectangle());
//		System.out.println("Circle with normal border");
//		circle.draw();
//
//		System.out.println("\nCircle of red border");
//		redCircle.draw();
//
//		System.out.println("\nRectangle of red border");
//		redRectangle.draw();
//		
		Shape triangleYelow = new YellowShapeDecorator(new Triangle());
		triangleYelow.draw();
		System.out.println("-----");
		Shape triangleYellowSmall = new SmallSizeDecorator(triangleYelow);
		triangleYellowSmall.draw();

	}

}
