package patterns.decorator.shapes;

public abstract class ShapeDecorator implements Shape {
	protected Shape decorator;

	public ShapeDecorator(Shape decoratedShape) {
		this.decorator = decoratedShape;
	}

	public void draw() {
		decorator.draw();
	}

}
