package patterns.decorator.shapes;

public class SmallSizeDecorator implements SizeDecorator {
	
	private Shape shape;
	
	public SmallSizeDecorator(Shape shape) {
		this.shape = shape;
	}

	public void draw() {
		shape.draw();
		printSize();
		
	}
	private void printSize( ) {
		System.out.println("It has a small size!");
	}

}
