package patterns.decorator.shapes;

public interface Shape {
	
	public void draw();

}
