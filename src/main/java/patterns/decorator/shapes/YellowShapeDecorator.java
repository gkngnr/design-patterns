package patterns.decorator.shapes;

public class YellowShapeDecorator extends ShapeDecorator {

	public YellowShapeDecorator(Shape decoratedShape) {
		super(decoratedShape);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void draw() {
		decorator.draw();
		setRedBorder(decorator);
	}
	
	private void setRedBorder(Shape deco) {
		System.out.println("Border Color: Yellow");
	}

}
