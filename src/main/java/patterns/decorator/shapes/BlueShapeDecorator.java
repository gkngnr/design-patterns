package patterns.decorator.shapes;

public class BlueShapeDecorator extends ShapeDecorator {

	public BlueShapeDecorator(Shape decoratedShape) {
		super(decoratedShape);
	}

	@Override
	public void draw() {
		decorator.draw();
		setRedBorder(decorator);
	}

	private void setRedBorder(Shape decoratedShape) {
		System.out.println("Border Color: Blue");
	}

}
