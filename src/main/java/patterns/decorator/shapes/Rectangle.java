package patterns.decorator.shapes;

public class Rectangle implements Shape {

	public void draw() {
		System.out.println("Shape: Rectangle");
		
	}

}
