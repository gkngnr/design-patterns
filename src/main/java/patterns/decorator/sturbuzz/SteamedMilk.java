package patterns.decorator.sturbuzz;

public class SteamedMilk extends ToppingDecorator {
	
	Beverage beverage;
	
	public SteamedMilk(Beverage beverage, Size size) {
		this.beverage = beverage;
		setSize(size);
	}

	@Override
	public String getDescription() {
		return beverage.getDescription() + ", " + size.name() + " Steamed Milk";
	}

	@Override
	public double cost() {
		double retVal;	
		switch (size) {
		case TALL:
			retVal = 0.10;
			break;
		case VENTI:
			retVal = 0.20;
			break;
		case GRANDE:
			retVal = 0.30;
			break;
		default:
			retVal = 0.10;
			break;
		}
		return beverage.cost() + retVal;
	}

}
