package patterns.decorator.sturbuzz;

public class HouseBlend extends Beverage{
	
	public HouseBlend(Size size) {
		description = size.name() + " HouseBlend";
		setSize(size);
	}

	@Override
	public double cost() {
		double retVal;
		switch (size) {
		case TALL:
			retVal = 0.89;
			break;
		case VENTI:
			retVal = 1.49;
			break;
		case GRANDE:
			retVal = 2.99;
			break;
		default:
			retVal = 1.89;
			break;
		}
		return retVal;
	}

}
