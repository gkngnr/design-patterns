package patterns.decorator.sturbuzz;

public class Espresso extends Beverage{
	
	public Espresso(Size size) {
		description = size.name() + " Espresso";
		setSize(size);
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		double retVal;
		switch (size) {
		case TALL:
			retVal = 1.99;
			break;
		case VENTI:
			retVal = 2.49;
			break;
		case GRANDE:
			retVal = 2.99;
			break;
		default:
			retVal = 1.99;
			break;
		}
		return retVal;
	}

}
