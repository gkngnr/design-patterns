package patterns.decorator.sturbuzz;

public class StarbuzzTest {

	public static void main(String[] args) {
		Beverage beverage = new Espresso(Size.TALL);
		System.out.println(beverage.getDescription()
		+ " $" + beverage.cost());
		Beverage beverage2 = new DarkRoast(Size.VENTI);
		beverage2 = new Mocha(beverage2, Size.VENTI);
		beverage2 = new Mocha(beverage2, Size.GRANDE);
		beverage2 = new Whip(beverage2, Size.VENTI);
		System.out.println(beverage2.getDescription()
		+ " $" + beverage2.cost());
		Beverage beverage3 = new HouseBlend(Size.TALL);
		beverage3 = new Soy(beverage3, Size.GRANDE);
		beverage3 = new Mocha(beverage3, Size.VENTI);
		beverage3 = new Whip(beverage3, Size.VENTI);
		System.out.println(beverage3.getDescription()
		+ " $" + beverage3.cost());

	}

}
