package patterns.decorator.sturbuzz;

public class Decaf extends Beverage{
	
	public Decaf(Size size) {
		description = size.name() + " Decaf";
		setSize(size);
	}

	@Override
	public double cost() {
		double retVal;
		switch (size) {
		case TALL:
			retVal = 1.05;
			break;
		case VENTI:
			retVal = 1.49;
			break;
		case GRANDE:
			retVal = 1.99;
			break;
		default:
			retVal = 1.09;
			break;
		}
		return retVal;
	}

}
