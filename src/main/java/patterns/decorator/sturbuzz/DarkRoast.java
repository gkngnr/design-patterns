package patterns.decorator.sturbuzz;

public class DarkRoast extends Beverage{
	
	public DarkRoast(Size size) {
		description = size.name() + " Dark Roast";
		setSize(size);
	}

	@Override
	public double cost() {
		double retVal;
		switch (size) {
		case TALL:
			retVal = 0.99;
			break;
		case VENTI:
			retVal = 1.49;
			break;
		case GRANDE:
			retVal = 1.99;
			break;
		default:
			retVal = 0.99;
			break;
		}
		return retVal;
	}

}
