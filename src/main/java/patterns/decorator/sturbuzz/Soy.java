package patterns.decorator.sturbuzz;

public class Soy extends ToppingDecorator {
	
	Beverage beverage;
	
	public Soy(Beverage beverage, Size size) {
		this.beverage = beverage;
		setSize(size);
	}

	@Override
	public String getDescription() {
		return beverage.getDescription() + ", " + size.name() +  " Soy";
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return beverage.cost() + 0.15;
	}

}
