package patterns.decorator.sturbuzz;

public abstract class ToppingDecorator extends Beverage{
	public abstract String getDescription();
}
