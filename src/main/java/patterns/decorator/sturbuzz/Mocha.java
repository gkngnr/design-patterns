package patterns.decorator.sturbuzz;

public class Mocha extends ToppingDecorator {
	
	Beverage beverage;
	
	public Mocha(Beverage beverage, Size size) {
		this.beverage = beverage;
		setSize(size);
	}

	@Override
	public String getDescription() {
		return beverage.getDescription() + ", " + size.name() + " Mocha";
	}

	@Override
	public double cost() {
		double retVal;
		switch (size) {
		case TALL:
			retVal = 0.20;
			break;
		case VENTI:
			retVal = 0.40;
			break;
		case GRANDE:
			retVal = 0.60;
			break;
		default:
			retVal = 0.20;
			break;
		}
		return beverage.cost() + retVal;
	}

}
