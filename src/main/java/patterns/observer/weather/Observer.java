package patterns.observer.weather;

public interface Observer {
	void update(Observable temp);

}
