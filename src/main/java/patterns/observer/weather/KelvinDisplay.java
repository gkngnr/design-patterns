package patterns.observer.weather;

public class KelvinDisplay implements Displayable {

	public void display(int temp) {
		System.out.println("Kelvin temperature: " + (temp + 273.15));		
	}

}
