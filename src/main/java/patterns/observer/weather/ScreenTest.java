package patterns.observer.weather;

public class ScreenTest {

	public static void main(String[] args) {
		
		WeatherBroadcast broadcast = new WeatherBroadcast();
		Observer screen1 = new Screen(new CelciusDisplay());
		Observer screen2 = new Screen(new FahrenheitDisplay());
		Observer screen3 = new Screen(new KelvinDisplay());
		
		broadcast.addSubscriber(screen1);
		broadcast.addSubscriber(screen2);
		broadcast.addSubscriber(screen3);
		
		broadcast.setTemp(10);
		
		broadcast.removeSubscriber(screen2);
		broadcast.setTemp(20);

	}

}
