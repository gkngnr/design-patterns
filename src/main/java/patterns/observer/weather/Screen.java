package patterns.observer.weather;

public class Screen implements Observer{
	private Displayable displayable;
	
	public Screen(Displayable displayable) {
		this.displayable = displayable;
	}

	public void update(Observable observable) {
		if(observable instanceof WeatherBroadcast) {
			WeatherBroadcast w = (WeatherBroadcast) observable;
			displayable.display(w.getTemp());
		}
			
		
	}

}
