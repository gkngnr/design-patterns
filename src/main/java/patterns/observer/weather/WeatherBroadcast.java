package patterns.observer.weather;

import java.util.ArrayList;
import java.util.List;

public class WeatherBroadcast implements Observable {
	private int temp;
	private List<Observer> subsList;
	
	public WeatherBroadcast() {
		subsList = new ArrayList<Observer>();
	}

	public void addSubscriber(Observer obs) {
		subsList.add(obs);		
	}

	public void removeSubscriber(Observer obs) {
		subsList.remove(obs);		
	}

	public void notifyUsers() {
		for(Observer obs : subsList) {
			obs.update(this);
		}
	}
	
	public void setTemp(int temp) {
		this.temp = temp;
		notifyUsers();
	}

	public int getTemp() {
		return temp;
	}
	
	

}
