package patterns.observer.weather;

public class CelciusDisplay implements Displayable {

	public void display(int temp) {
		System.out.println("Celcius temperature: " + temp);		
	}

}
