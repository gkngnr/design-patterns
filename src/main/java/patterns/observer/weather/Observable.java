package patterns.observer.weather;

public interface Observable {
	void addSubscriber(Observer obs);
	void removeSubscriber(Observer obs);
	void notifyUsers();

}
