package patterns.observer.weather;

public interface Displayable {
	void display(int temp);
}
