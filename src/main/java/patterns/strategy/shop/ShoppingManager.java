package patterns.strategy.shop;

public class ShoppingManager {
	
	public void pay(PaymentStrategy paymentStrategy, int amount) {
		paymentStrategy.pay(amount);
	}

}
