package patterns.strategy.shop;

public interface PaymentStrategy {
	void pay(int amount);

}
