package patterns.strategy.shop;

public class CreditCardPayment implements PaymentStrategy {
	
	private String cardNo;
	private String ccv;
	private String expiryDate;
	private String name;	
	

	public CreditCardPayment(String cardNo, String ccv, String expiryDate, String name) {
		this.cardNo = cardNo;
		this.ccv = ccv;
		this.expiryDate = expiryDate;
		this.name = name;
	}

	public void pay(int amount) {
		System.out.println("Payment with credit card " + amount);		
	}

}
