package patterns.strategy.shop;

public class ShoppingTest {

	public static void main(String[] args) {
		Item item1 = new Item("111", 100);
		Item item2 = new Item("222", 200);
		Item item3 = new Item("333", 300);
		ShoppingCart cart = new ShoppingCart();
		cart.addItem(item3);
		cart.addItem(item2);
		cart.addItem(item1);
		cart.removeItem(item3);		
		ShoppingManager manager = new ShoppingManager();
		manager.pay(new CreditCardPayment("cardNo", "ccv", "expiryDate", "name"), cart.calculateTotal());
		manager.pay(new PaypalPayment("name", "email"), cart.calculateTotal());

	}

}
