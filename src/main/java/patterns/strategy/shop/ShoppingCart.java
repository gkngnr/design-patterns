package patterns.strategy.shop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShoppingCart {
	List<Item> itemList;
	
	public ShoppingCart() {
		itemList = new ArrayList<Item>();
	}
	
	public void addItem(Item item) {
		itemList.add(item);
	}
	public void removeItem(Item item) {
		int index = itemList.indexOf(item);
		if (index != -1) {
			itemList.remove(index);
		}
	}
	public int calculateTotal() {
		int count = 0;
		for (Item item : itemList) {			
			count += item.getPrice();
		}
		return count;
	}	

}
