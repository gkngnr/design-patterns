package patterns.strategy.duckapp;

public class QuackNo implements QuackBehavior{

	public void quack() {
		System.out.println("Quacking noo");
		
	}

}
