package patterns.strategy.duckapp;

public class QuackHigh implements QuackBehavior {

	public void quack() {
		System.out.println("Quackin hiiigh");
		
	}

}
