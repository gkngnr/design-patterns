package patterns.strategy.duckapp;

public class SpeedDuck extends Duck{
	public SpeedDuck(){
		flyBehavior = new FlyHigh();
		quackBehavior = new QuackHigh();
	}

}
