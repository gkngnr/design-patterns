package patterns.strategy.duckapp;

public class RubberDuck extends Duck {
	public RubberDuck() {
		flyBehavior = new FlyNo();
		quackBehavior = new QuackNo();
	}

}
