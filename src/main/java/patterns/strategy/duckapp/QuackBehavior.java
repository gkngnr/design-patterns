package patterns.strategy.duckapp;

public interface QuackBehavior {
	
	public void quack(); 

}
