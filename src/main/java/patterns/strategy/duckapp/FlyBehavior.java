package patterns.strategy.duckapp;

public interface FlyBehavior {
	
	public void fly();
}
