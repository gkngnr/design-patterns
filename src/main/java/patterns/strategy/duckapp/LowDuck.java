package patterns.strategy.duckapp;

public class LowDuck extends Duck {
	public LowDuck() {
		flyBehavior = new FlyLow();
		quackBehavior = new QuackLow();
	}
}
