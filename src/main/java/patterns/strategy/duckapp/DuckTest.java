package patterns.strategy.duckapp;

public class DuckTest {

	public static void main(String[] args) {
		System.out.println("now coming high duck:");
		Duck speedDuck = new SpeedDuck();
		speedDuck.fly();
		speedDuck.quack();
		System.out.println("now coming low duck:");
		Duck lowDuck = new LowDuck();
		lowDuck.fly();
		lowDuck.quack();
		System.out.println("now coming rubber duck:");
		Duck rubberDuck = new RubberDuck();
		rubberDuck.fly();
		rubberDuck.quack();
		System.out.println("now coming new version of rubber duck:");
		rubberDuck.setQuackBehavior(new QuackRocket());
		rubberDuck.quack();

	}

}
